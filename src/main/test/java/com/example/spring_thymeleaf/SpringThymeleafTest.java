package com.example.spring_thymeleaf;
import com.example.spring_thymeleaf.entities.LapTime;
import com.example.spring_thymeleaf.repo.LapTimeRepo;
import com.example.spring_thymeleaf.service.LapTimeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SpringThymeleafTest {

    @Mock
    LapTimeRepo lapTimeRepo;

    @Test
    public void listAllLaptimesTest(){
        //Given
        double[] times = new double[]{15.4, 84.4, 45.23, 48.9, 13.5, 18.2};
        List<LapTime> unsortedLapTimes = new ArrayList<>();

        LapTimeService lapTimeService = new LapTimeService(lapTimeRepo);

        for(double time: times) {
            LapTime lapTime = new LapTime();
            lapTime.setLapTime(time);
            unsortedLapTimes.add(lapTime);
        }

        List<LapTime> sortedLapTimes = new ArrayList<>();
        sortedLapTimes.add(unsortedLapTimes.get(4));
        sortedLapTimes.add(unsortedLapTimes.get(0));
        sortedLapTimes.add(unsortedLapTimes.get(5));
        sortedLapTimes.add(unsortedLapTimes.get(2));
        sortedLapTimes.add(unsortedLapTimes.get(3));

        //When
        when(lapTimeRepo.findAll()).thenReturn(unsortedLapTimes);
        List<LapTime> resultLapTimes = lapTimeService.findLapTimes();

        //Then
        assertIterableEquals(sortedLapTimes, resultLapTimes);
    }

}
