package com.example.spring_thymeleaf;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AutomationTest {
    private WebDriver driver;
    private final static String baseUrl = "http://localhost:8080/laptimes";

    @BeforeClass
    public void setup(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void init(){
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(option);
        driver.manage().window().maximize();
        driver.get(baseUrl);
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void addLaptimeTest(){
        driver.findElement(By.id("addtodo-button")).click();
        driver.findElement(By.id("input-laptime")).sendKeys("54");
        driver.findElement(By.id("save-button")).click();

        WebElement laptimeClassElement = driver.findElement(By.className("lapTime-list"));


        Assert.assertTrue(laptimeClassElement.getText().contains("Varvtid: 54.0"));
    }

    @Test
    public void addMultipleLaptimesTest(){
        driver.findElement(By.id("addtodo-button")).click();
        driver.findElement(By.id("input-laptime")).sendKeys("54");
        driver.findElement(By.id("save-button")).click();
        driver.findElement(By.id("addtodo-button")).click();
        driver.findElement(By.id("input-laptime")).sendKeys("45");
        driver.findElement(By.id("save-button")).click();

        int numberOfLaptimes = driver.findElements(By.className("lapTime-list")).size();
        Assert.assertTrue(numberOfLaptimes>2);
        System.out.println(numberOfLaptimes);
    }

    @Test
    public void checkOnlyFiveLaptimesShow(){

        for (int i = 0; i < 6; i++) {
            String laptimeValue = String.valueOf(i + i * 2);
            driver.findElement(By.id("addtodo-button")).click();
            driver.findElement(By.id("input-laptime")).sendKeys(laptimeValue);
            driver.findElement(By.id("save-button")).click();
        }

        int numberOfLaptimes = driver.findElements(By.className("lapTime-list")).size();
        Assert.assertEquals(numberOfLaptimes, 5);
    }




}
